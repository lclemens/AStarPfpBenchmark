//using Unity.Transforms;
//using Unity.Entities;
//using Unity.Burst;
//using Unity.Collections;
//using Unity.Mathematics;

//[WithAll(typeof(EnableMovementTag)), WithNone(typeof(RvoAgentData))]
//[BurstCompile]
//public partial struct BasicPathMovementJob : IJobEntity
//{
//    [ReadOnly] public float DeltaTime;

//    // Loops through all agents and sets their next position (pos.Value),
//    // and at the same time it updates the RVO simulation for each agent.
//    [BurstCompile]
//    public void Execute(ref LocalTransform lt, ref WaypointIndexData waypoint, ref TravelStateData travel, in MoveSpeedData speed, in DynamicBuffer<WaypointData> path)
//    {
//        if (path.Length <= 0) { return; }
//        if (travel.HasReachedEndOfPath) { return; }

//        WaypointSelectResult result = PathUtils.SelectWaypoint(lt.Position, 6f, waypoint.Index, path);

//        // only set the target again if the next waypoint has changed
//        if ((waypoint.Index == 0) || (waypoint.Index != result.CurrentIndex)) {
//            // set the new waypoint index
//            waypoint.Index = result.CurrentIndex;
//            travel.HasReachedEndOfPath = result.HasReachedEndOfPath;
//            // if this is the end of the path, then we should return (the code below wont make sense because the target waypoint
//            // will be very close or the same as the current position and that will give a bad direction.
//            if (travel.HasReachedEndOfPath) { return; }

//            //DebugDrawUtils.DrawCross(curWaypointPos, 0.8f, UnityEngine.Color.white, 5f);
//        }

//        // This is the point and speed that the RVO agent should move to, as dicatated by the RVO sim.
//        // If it's the only agent around, then this point will likely be the same as the current waypoint position.
//        // However, if there are a lot of other agents or obstacles in the vicinity, then this point will likely
//        // be different so that the agent can avoid bumping into other agents or obstacles.
//        // Note that this point has an incorrect height (y value) that doesn't match the terrain. I don't know why.
//        // This point is the same as rvoAgent.CalculatedTargetPoint.
//        float3 targetPt = path[waypoint.Index].Point;

//        // Calculate the direction to the next calculated target point.
//        float3 dir = math.normalize(targetPt - lt.Position);

//        // Set the entity's rotation to face in the direction of movement.
//        lt.Rotation = quaternion.LookRotation(dir, math.up());

//        // Move the entity in the necessary direction by the amount at delta-time multiplied with the calculated speed.
//        lt.Position += (dir * DeltaTime * speed.Speed);
//    }
//}

//// systembase must be used instead of isystem because m_simBurst is a managed object
//public partial class BasicPathMovementSystem : SystemBase
//{
//    Pathfinding.RVO.SimulatorBurst m_simBurst;

//    protected override void OnStartRunning()
//    {
//        m_simBurst = UnityEngine.GameObject.Find("RvoSimulator").GetComponent<Pathfinding.RVO.RVOSimulator>().GetSimulator();
//    }

//    protected override void OnUpdate()
//    {
//        BasicPathMovementJob job = new BasicPathMovementJob();
//        job.DeltaTime = SystemAPI.Time.DeltaTime;
//        job.ScheduleParallel();
//    }
//}