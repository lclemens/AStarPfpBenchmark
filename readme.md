This is for benchmarking the A\* Pathfinding Project asset with RVO in pure ECS mode (DOTS).

You must own the asset and import it separately.
https://assetstore.unity.com/packages/tools/ai/a-pathfinding-project-pro-87744

There are two scenes.
<ol>
	<li><b>BenchmarkScene.unity</b> - This scene uses the old ECS mode before FollowerEntity existed. I had to rewrite a ton of code to be ECS compatible and kludge together the RVO simulation to synchronize with entities.</li>
	<li><b>FollowerBenchmarkScene.unity</b> - This scene uses the new FollowerEntity mode. I made an authoring/baker and an initialization script that can be placed on a game object in a subscene to turn it into an agent entity.</li>
<ol>
<br>
<hr>

![screenshot](Images/screenshot.png)